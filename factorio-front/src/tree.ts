export class TreeStep {
    id: number | undefined;
    name: string | undefined;
    count: number | undefined;
    constructor(obj: { id: number, name: string, count: number }) {
        this.id = obj.id;
        this.name = obj.name;
        this.count = obj.count;
    }
};
export class Tree {
    target: string;
    target_count: number;
    tree: { [key: string]: TreeStep[] };
    raw_resources: { [key: string]: number };
    constructor(obj: { target: string, target_count: number, tree: { [key: string]: { id: number, name: string, count: number }[] }, raw_resources: { [key: string]: number } }) {
        this.target = obj.target;
        this.target_count = obj.target_count;
        this.tree = {}
        for (let step in obj.tree) {
            const step_array = obj.tree[step];
            this.tree[step] = []
            for (let s of step_array) {
                this.tree[step].push(new TreeStep(s))
            }
        }
        this.raw_resources = {}
        for (let resource in obj.raw_resources) {
            this.raw_resources[resource] = obj.raw_resources[resource];
        }

    }
}